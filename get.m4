divert(-1)
include(`wasora/m4/getscript.m4')
define(name, fino)
define(petsc_version, 3.8.3)
divert(0)dnl
GET_HEADER
GET_STEP0
GET_STEP1
GET_STEP2
GET_STEP3
GET_STEP4
GET_PETSC
GET_STEP5
GET_STEP6
# god forbids...
if [ ! -z "`uname | grep CYGWIN`" ]; then
  ln -s libs/petsc-petsc_version/arch-linux2-c-opt/lib/libpetsc-petsc_version
fi
GET_STEP7
GET_STEP8
GET_FOOTER
