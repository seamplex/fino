changequote([!,!])dnl
% Fino reference sheet
% Jeremy Theler

This reference sheet is for [Fino](index.html) esyscmd([!git describe | sed 's/-/./'!]). 
Note that Fino works on top of [wasora](https://www.seamplex.com/wasora), so you should also check the [wasora reference sheet](https://www.seamplex.com/wasora/reference.html) also---not to mention the [wasora RealBook](https://www.seamplex.com/wasora/realbook).

# Keywords

esyscmd([!../../wasora/doc/reference.sh parser kw!])

# Variables

esyscmd([!../../wasora/doc/reference.sh init va!])

